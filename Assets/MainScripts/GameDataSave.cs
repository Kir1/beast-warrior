﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class GameDataSave
{
    public static GameDataSave current;
    public int currentLevel = 1;
    public int level = 1;
    public int deathCount = 0;
    public string lang = "ru";
}