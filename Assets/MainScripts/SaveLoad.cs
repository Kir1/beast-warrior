﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveLoad
{
    //методы загрузки и сохранения статические, поэтому их можно вызвать откуда угодно
    public static void Save()
    {
        string destination = Application.persistentDataPath + "/save1.dat";
        FileStream file;

        if (File.Exists(destination)) file = File.OpenWrite(destination);
        else file = File.Create(destination);

        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(file, GameDataSave.current);
        file.Close();
    }

    public static bool Load()
    {
        string destination = Application.persistentDataPath + "/save1.dat";

        FileStream file;

        if (File.Exists(destination))
        {
            file = File.OpenRead(destination);
            BinaryFormatter bf = new BinaryFormatter();

            GameDataSave data = (GameDataSave)bf.Deserialize(file);
            file.Close();

            GameDataSave.current = data;

            return true;
        }
        else
        {
            Debug.Log("fls load");

            return false;
        }
   
    }

    public static bool IfGameContinue()
    {
        return GameDataSave.current.currentLevel > 1;
    }
}