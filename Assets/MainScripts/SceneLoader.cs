﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    public Text m_Text;
    public Button m_Button;
    public string sceneName;

    public void LoadLevel()
    {
        gameObject.SetActive(true);
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        yield return null;

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync("Level" + GameDataSave.current.currentLevel);
        asyncOperation.allowSceneActivation = false;
  
        while (!asyncOperation.isDone)
        {
            m_Text.text = "Loading progress: " + (asyncOperation.progress * 100) + "%";

            if (asyncOperation.progress >= 0.9f)
            {
                m_Text.text = "Press the space bar to continue";
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    gameObject.SetActive(false);
                    asyncOperation.allowSceneActivation = true;
                }
            }

            yield return null;
        }
    }
}