﻿using System.Collections.Generic;


using UnityEngine;
using System.Collections;

class Language {
    public static string text = "";

    public static string translate(string text)
    {
        var lang = GameDataSave.current.lang;
        Language.text = text;
        if (lang == "ru") Language.getText(LanRu.text);
        if (lang == "en") Language.getText(LanEn.text);
        return Language.text;
    }

    public static void getText(Dictionary<string, string> dictonary)
    {
      if (dictonary.ContainsKey(Language.text))
            Language.text = dictonary[Language.text];
      else
            Language.text = "Not translate";
    }
}
