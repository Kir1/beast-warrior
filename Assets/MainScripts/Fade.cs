﻿namespace Game
{
    using UnityEngine;
    using UnityEngine.UI;
    using System.Collections;

    public class Fade : MonoBehaviour
    {
        public void StartProcess(FadeStartDelegate fadeStartDelegate, FadeMiddleDelegate fadeMiddleDelegat, FadeEndDelegate fadeEndDelegate)
        {
            gameObject.SetActive(true);
            fadeStartDelegate();
            StartCoroutine(FadeStart(fadeMiddleDelegat, fadeEndDelegate));
        }

        IEnumerator FadeStart(FadeMiddleDelegate fadeMiddleDelegat, FadeEndDelegate fadeEndDelegate)
        {
            byte aplha = 0;
            while (true)
            {
                yield return null;
                if (aplha == 255)
                {
                    fadeMiddleDelegat();
                    StartCoroutine(FadeEnd(fadeEndDelegate));
                    break;
                }
                gameObject.GetComponent<Image>().color = new Color32(0, 0, 0, aplha);
                aplha+=5;
            }
        }

        IEnumerator FadeEnd(FadeEndDelegate fadeEndDelegate)
        {
            byte aplha = 255;
            while (true)
            {
                yield return null;
                if (aplha == 0)
                {
                    fadeEndDelegate();
                    gameObject.SetActive(false);
                    break;
                }
                gameObject.GetComponent<Image>().color = new Color32(0, 0, 0, aplha);
                aplha-=5;
            }
        }

    }


}