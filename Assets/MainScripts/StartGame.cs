﻿namespace Game
{
    using UnityEngine;
    using UnityEngine.UI;

    public delegate void FadeStartDelegate();
    public delegate void FadeMiddleDelegate();
    public delegate void FadeEndDelegate();

    //начало игры - срабатывает при запуске игры
    public class StartGame : MonoBehaviour
    {
        public GameObject sceneLoader;
        
        public GameObject fade;

        public GameObject prefubTitle;
        public GameObject prefubItem;
        public GameObject prefubItemGame;
        public GameObject prefubItemExit;
        public GameObject prefubPopoverText;
        public GameObject prefubPopoverList;
        public GameObject prefubPopoverListItem;

        public GameObject menuMain;
        public GameObject menuFAQ;
        public GameObject menuHelpers;
        public GameObject menuBestiary;
        public GameObject menuHeroes;
        public GameObject menuTraps;

        void Start()
        {
            GameManager.sceneLoader = sceneLoader;

            if (SaveLoad.Load() == false)
            {
                GameDataSave.current = new GameDataSave();
                SaveLoad.Save();
            }

            DontDestroyOnLoad(this.gameObject);

            ////////
            AddTitle("name_game", menuMain);

            AddFirstItemGame();

            AddItemPopoverText("faq", menuMain, menuFAQ, "faq", "faq.text");

            PopoverListItemObject[] monstersItems = {
                new PopoverListItemObject("zombi", "zombi.descr", Resources.Load<Sprite>("Monsters/Zombie/walk"), 1),
                new PopoverListItemObject("runaway", "runaway.descr", Resources.Load<Sprite>("Monsters/Runaway/stand"), 5)
            };
            AddItemPopoverList("bestiary", menuMain, menuBestiary, "bestiary", prefubPopoverListItem, monstersItems);

            AddItemExit();

        }


        public GameObject CreateObject(GameObject prefub, GameObject parent)
        {
            return Instantiate(prefub, parent.transform);
        }

        public GameObject AddTitle(string title, GameObject parent)
        {
            var textobj = CreateObject(prefubTitle, parent);
            textobj.GetComponent<Text>().text = Language.translate(title);
            return textobj;
        }

        public GameObject AddItem(string title, GameObject parent, GameObject showObject)
        {
            var textobj = CreateObject(prefubItem, parent);
            textobj.GetComponent<Text>().text = Language.translate(title);
            textobj.GetComponent<Item>().item = showObject;
            return textobj;
        }

        public GameObject AddFirstItemGame()
        {
            var text = "start_game";
            if (SaveLoad.IfGameContinue()) text = "continue_game";
            var textobj = CreateObject(prefubItemGame, menuMain);
            textobj.GetComponent<Text>().text = Language.translate(text);
            return textobj;
        }

        public GameObject AddItemExit()
        {
            var textobj = CreateObject(prefubItemExit, menuMain);
            textobj.GetComponent<Text>().text = Language.translate("exit");
            return textobj;
        }

        public GameObject AddItemPopoverText(string title, GameObject parent, GameObject popoverContainer, string popoverTitle, string popoverText)
        {
            var obj = AddItem(title, parent, popoverContainer);
            var popover = CreateObject(prefubPopoverText, popoverContainer);
            popover.GetComponent<PopoverText>().Constructor(popoverTitle, popoverText, popoverContainer);
            return obj;
        }

        public GameObject AddItemPopoverList(
            string title,
            GameObject parent,
            GameObject popoverContainer,
            string popoverTitle,
            GameObject prefubPopoverListItem,
            PopoverListItemObject[] items)
        {
            var obj = AddItem(title, parent, popoverContainer);
            var popover = CreateObject(prefubPopoverList, popoverContainer);
            popover.GetComponent<PopoverList>().Constructor(popoverTitle, prefubPopoverListItem, items, popoverContainer);
            return obj;
        }
    }


}