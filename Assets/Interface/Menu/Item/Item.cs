﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Item : MonoBehaviour, IPointerClickHandler {

    public GameObject item;

    public void OnPointerClick(PointerEventData eventData)
    {
        ClickAction();
    }

    virtual public void ClickAction()
    {
        item.SetActive(true);
    }
}
