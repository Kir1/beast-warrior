﻿using UnityEngine;

public class ItemExit : Item {

    public override void ClickAction()
    {
        Application.Quit();
    }
}
