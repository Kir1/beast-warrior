﻿using UnityEngine.UI;
using UnityEngine;

public class PopoverList : MonoBehaviour {

    public GameObject hideObject;

    public void Constructor(string title, GameObject prefubListItem, PopoverListItemObject[] items, GameObject hideObject) {
       var titleComponent = gameObject.transform.Find("Title");
       titleComponent.GetComponent<Text>().text = Language.translate(title);
       
       var firstObject = items[0];
    
       var imageComponent = gameObject.transform.Find("Detail/Image");
       imageComponent.GetComponent<Image>().sprite = firstObject.image;
   
       var subtitleComponent = gameObject.transform.Find("Detail/Title");
       subtitleComponent.GetComponent<Text>().text = Language.translate(firstObject.title);

       var descrComponent = gameObject.transform.Find("Detail/TextArea/TextContainer/Content/Message");
       descrComponent.GetComponent<Text>().text = Language.translate(firstObject.descr);
     
       GameObject gameObjectPrefub;
       Transform detail = gameObject.transform.Find("Detail");
       foreach(PopoverListItemObject item in items)
       {
            gameObjectPrefub = Instantiate(prefubListItem, gameObject.transform.Find("List/Content").transform);
            gameObjectPrefub.transform.Find("Image").GetComponent<Image>().sprite = item.image;
            gameObjectPrefub.GetComponent<PopoverListItem>().title = item.title;
            gameObjectPrefub.GetComponent<PopoverListItem>().descr = item.descr;
            gameObjectPrefub.GetComponent<PopoverListItem>().detail = detail;
            gameObjectPrefub.GetComponent<PopoverListItem>().lvl = item.lvl;
        }
       
       this.hideObject = hideObject;
    }

    public void Close()
    {
        hideObject.SetActive(false);
    }
}
