﻿using UnityEngine;

public class PopoverListItemObject {
    public string title;
    public string descr;
    public Sprite image;
    public int lvl;

    public PopoverListItemObject(string title, string descr, Sprite image, int lvl = 1)
    {
        this.title = title;
        this.descr = descr;
        this.image = image;
        this.lvl = lvl;
    }
}
