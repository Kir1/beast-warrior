﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PopoverListItem : MonoBehaviour, IPointerClickHandler {
    public string title;
    public string descr;
    public Transform detail;
    public int lvl = 1;
    public Image image;

    void Start()
    {
      image = gameObject.transform.Find("Image").GetComponent<Image>();
    }

    void Update()
    {
        if (checkAcces())
            image.color = Color.white;
        else
            image.color = Color.black;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        ClickAction();
    }

    bool checkAcces()
    {
        return GameDataSave.current.currentLevel >= lvl;
    }

    virtual public void ClickAction()
    {
        if (checkAcces())
        {
            detail.Find("Image").GetComponent<Image>().sprite = image.sprite;
            detail.Find("Title").GetComponent<Text>().text = Language.translate(title);
            detail.Find("TextArea/TextContainer/Content/Message").GetComponent<Text>().text = Language.translate(descr);
        }
    }
}
