﻿using UnityEngine.UI;
using UnityEngine;

public class PopoverText : MonoBehaviour {
    public GameObject hideObject;

    public void Constructor(string title, string text, GameObject hideObject)
    {
        var titleComponent = gameObject.transform.Find("Container/Title");
        titleComponent.GetComponent<Text>().text = Language.translate(title);
        var textComponent = gameObject.transform.Find("Container/Body/Content/Message");
        textComponent.GetComponent<Text>().text = Language.translate(text);
        this.hideObject = hideObject;
    }

    public void Close()
    {
        hideObject.SetActive(false);
    }
}
